var shelf = /* initialize bookshelf here */

app.use(function(req, res, next) {
	req.db = shelf;
	req.model = shelf.model.bind(shelf);
	return next();
});